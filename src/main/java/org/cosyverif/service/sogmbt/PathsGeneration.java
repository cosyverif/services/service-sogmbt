package org.cosyverif.service.sogmbt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.exec.CommandLine;
import org.cosyverif.alligator.service.BinaryService;
import org.cosyverif.alligator.service.AnnotatedService.Task;
import org.cosyverif.alligator.service.Parameter.Direction;
import org.cosyverif.alligator.service.annotation.Example;
import org.cosyverif.alligator.service.annotation.Launch;
import org.cosyverif.alligator.service.annotation.Parameter;
import org.cosyverif.alligator.service.annotation.Service;
import org.cosyverif.alligator.service.output.ParameterOutputHandler;
import org.cosyverif.model.Model;

@Service(name = "Test Paths Generation", help = "Symbolic Observation Graph-Based Generation of Test Paths", version = "1.0.0", tool = "sogmbt", authors = {
    "Jaime Arias",
    "Jörg Desel",
    "Kaïs Klai",
    "Hanen Ochi",
    "M. Taha Bennani"
}, keywords = {})
public final class PathsGeneration extends BinaryService {
  GrmlParser parser;

  // Model file
  @Parameter(name = "Model", help = "Petri net model", direction = Direction.IN, formalism = "http://formalisms.cosyverif.org/pt-net.fml")
  private Model model;

  // Number of places of the model
  @Parameter(name = "Number of places", help = "Number of places of the model", direction = Direction.OUT, key = "# places")
  public String nbPlaces = "";

  // Number of transitions of the model
  @Parameter(name = "Number of transitions", help = "Number of transitions of the model", direction = Direction.OUT, key = "# transition")
  public int nbTransitions;

  // Number of observable transitions of the model
  @Parameter(name = "Number of observable transitions", help = "Number of observable transitions of the model", direction = Direction.OUT, key = "# observable transitions")
  public int nbObservableTransitions;

  // Number of abstract paths
  @Parameter(name = "Number of abstract paths", help = "Number of abstract paths", direction = Direction.OUT, key = "# abstract paths")
  public int nbAbstractPaths;

  // Number of covered transitions
  @Parameter(name = "Number of convered transitions", help = "Number of covered transitions", direction = Direction.OUT, key = "# covered transitions")
  public int nbCoveredTransitions;

  // Number of SOG aggregates
  @Parameter(name = "Number of SOG aggregates", help = "Number of SOG aggregates", direction = Direction.OUT, key = "NB NODES")
  public int nbAggregates;

  // Number of SOG transitions
  @Parameter(name = "Number of SOG transitions", help = "Number of SOG transitions", direction = Direction.OUT, key = "NB ARCS")
  public int nbSOGTransitions;

  // observable paths file
  @Parameter(name = "Observable Paths", help = "", direction = Direction.OUT, contenttype = "plain/text")
  public File outputFile = null;

  @Launch
  public Task executeBinary() throws IOException {
    parser = GrmlParser.create(model);
    final File model = parser.parse();

    final CommandLine command = new CommandLine("assets/sogMBT");
    command.addArguments("--input-net " + model.getAbsolutePath());
    command.addArguments("--output-folder .");

    final Path outputFilePath = Paths.get(model.getParent(), "obs_paths_" + model.getName() + ".txt");

    return task(command)
        .workingDirectory(baseDirectory())
        .out(new ParameterOutputHandler(this) {
          @Override
          public void fallback(String line) {
            // return output file
            File f = new File(outputFilePath.toString());
            if (f.exists()) {
              outputFile = f;
            }
          }

          @Override
          public void fallback(String key, String value) {
          }
        });
  }

  @Example(name = "Example of path generation", help = "Example of path generation")
  public PathsGeneration example() {
    PathsGeneration service = new PathsGeneration();
    service.model = loadModelResource("/models/example.grml");
    return service;
  }
}
