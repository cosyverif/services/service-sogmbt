package org.cosyverif.service.sogmbt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;

import org.cosyverif.Configuration;
import org.cosyverif.model.Model;
import org.cosyverif.model.Node;

import org.cosyverif.model.Arc;
import org.cosyverif.model.Attribute;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.NONE)
@Accessors(chain = true, fluent = true)
public class GrmlParser {
  /** GRML Model */
  private Model model;

  /**
   * Constructor
   *
   * @param model Grml Model
   */
  public GrmlParser(Model model) {
    this.model = model;
  }

  /**
   * Creates a parser from Grml to ADTree
   *
   * @param model Grml Model
   * @return GrmlParser object
   */
  public static GrmlParser create(Model model) {
    return new GrmlParser(model);
  }

  /**
   * Generates a file handled by the adt2amas tool
   *
   * @return File object of the generated file
   * @throws IOException
   */
  public File parse() throws IOException {
    /** Create .net input file */
    File outputDirectory = Configuration.instance().temporaryDirectory();
    File fileOutput = File.createTempFile("sogmbt_", "", outputDirectory);
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput));

    /** Parse GrML into a Petri net model */
    Net net = parseNet();

    /** write file */
    writer.write(net.toImitator());
    writer.close();

    return fileOutput;
  }

  private Net parseNet() {

    /** Parsing model title */
    String title = "";
    for (Attribute attr : model.getAttribute()) {
      if (attr.getName().equals("title") && attr.getContent().size() == 1) {
        title = (String) attr.getContent().get(0);
      }
    }

    Net net = new Net(title);
    for (Node node : model.getNode()) {
      String id = node.getId().toString();
      String label = "";
      int marking = 0;

      // Parse attributes
      for (Object content : node.getNodeContent()) {
        Attribute attr = ((Attribute) content);
        List<Object> attributeContent = attr.getContent();

        String attributeName = attr.getName();
        if (attributeName.equals("name")) {
          // parse name
          label = attributeContent.get(0).toString();
        } else if (attributeName.equals("marking")) {
          // parse marking
          marking = Integer.valueOf(attributeContent.get(0).toString());
        }
      }

      // node is a place
      if (node.getNodeType().equals("place")) {
        net.addPlace(new Place(id, label, marking));
      } else if (node.getNodeType().equals("transition")) {
        net.addTransition(new Transition(id, label));
      }
    }

    for (Arc arc : model.getArc()) {
      if (arc.getArcType().equals("arc")) {
        String source = arc.getSource().toString();
        String target = arc.getTarget().toString();

        if (net.isTransition(target)) {
          // incoming arc
          net.getTransition(target).addIncomingArc(net.getPlace(source).label());
        } else if (net.isTransition(source)) {
          // outgoing arc
          net.getTransition(source).addOutgoingArc(net.getPlace(target).label());
        }
      }
    }

    return net;
  }

}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Place {
  private String id;
  private String label;
  private int marking;

  String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append("#place ").append(label);
    if (marking() > 0) {
      sb.append(" mk(").append(marking).append("<..>)");
    }
    return sb.toString();
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Transition {
  private String id;
  private String label;
  private ArrayList<String> outgoingArcs;
  private ArrayList<String> incomingArcs;

  public Transition(String id, String label) {
    this.id = id;
    this.label = label;
    this.outgoingArcs = new ArrayList<String>();
    this.incomingArcs = new ArrayList<String>();
  }

  public void addOutgoingArc(String id) {
    this.outgoingArcs.add(id);
  }

  public void addIncomingArc(String id) {
    this.incomingArcs.add(id);
  }

  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    sb.append("#trans ").append(label).append("\n");

    // incoming arcs
    if (incomingArcs.size() > 0) {
      sb.append("in {");
      for (String p : incomingArcs) {
        sb.append(p + ":<..>;");
      }
      sb.append("}\n");
    }

    // incoming arcs
    if (outgoingArcs.size() > 0) {
      sb.append("out {");
      for (String p : outgoingArcs) {
        sb.append(p + ":<..>;");
      }
      sb.append("}\n");
    }

    sb.append("#endtr");
    return sb.toString();
  }
}

@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Accessors(chain = true, fluent = true)
class Net {
  private String title;
  private HashMap<String, Place> places = new HashMap<String, Place>();
  private HashMap<String, Transition> transitions = new HashMap<String, Transition>();

  public Net(String title) {
    this.title = title;
  }

  public void addTransition(Transition t) {
    this.transitions.put(t.id(), t);
  }

  public void addPlace(Place p) {
    this.places.put(p.id(), p);
  }

  public Transition getTransition(String id) {
    return this.transitions.get(id);
  }

  public Place getPlace(String id) {
    return this.places.get(id);
  }

  public ArrayList<Transition> transitions() {
    return new ArrayList<Transition>(this.transitions.values());
  }

  public ArrayList<Place> places() {
    return new ArrayList<Place>(this.places.values());
  }

  public Boolean isTransition(String id) {
    return this.transitions.containsKey(id);
  }

  public Boolean isPlace(String id) {
    return this.places.containsKey(id);
  }

  public String toImitator() {
    StringBuilder sb = new StringBuilder();
    /** parse places */
    for (Place p : places()) {
      sb.append(p.toImitator()).append("\n");
    }

    /** parse transitions */
    for (Transition t : transitions()) {
      sb.append(t.toImitator()).append("\n");
    }

    return sb.toString();
  }

}