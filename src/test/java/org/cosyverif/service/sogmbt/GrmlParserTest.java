package org.cosyverif.service.sogmbt;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.cosyverif.Configuration;
import org.cosyverif.alligator.XML;
import org.cosyverif.alligator.service.util.FileUtility;
import org.cosyverif.alligator.util.Utility;
import org.cosyverif.model.Model;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public final class GrmlParserTest {
  private File directory;

  /** Create temp folder */
  @Before
  public void setUp() {
    directory = Utility.createTemporaryDirectory();
    Configuration.instance()
        .temporaryDirectory(directory);
  }

  /**
   * Loads a model from a GrML file in the classpath.
   *
   * @param path the path to the GrML file.
   * @return the model
   */
  public final Model loadModelResource(String path) {
    try {
      if (path.startsWith("/")) {
        path = path.substring(1);
      }

      File file = File.createTempFile("model_", ".grml", Configuration.instance().temporaryDirectory());
      FileUtility.copyFile(getClass().getClassLoader().getResource(path).openConnection().getURL(), file);
      Model result = (Model) XML.fromFile(file);
      file.delete();
      return result;
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Test
  public void testConstructor() {
    try {
      Model model = new Model();
      new GrmlParser(model);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail("Unable to instantiate the GrmlParser class.");
    }
  }

  @Test
  public void testParse() throws IOException {
    GrmlParser parser = new GrmlParser(loadModelResource("/models/example.grml"));
    File model = parser.parse();
  }
}
