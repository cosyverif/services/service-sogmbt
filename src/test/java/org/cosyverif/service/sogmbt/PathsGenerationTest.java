package org.cosyverif.service.sogmbt;

import java.io.File;

import org.cosyverif.Configuration;
import org.cosyverif.alligator.service.AnnotatedService;
import org.cosyverif.alligator.util.Utility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public final class PathsGenerationTest {
  private File directory;

  /** Create temp folder */
  @Before
  public void setUp() {
    directory = Utility.createTemporaryDirectory();
    Configuration.instance()
        .temporaryDirectory(directory);
  }

  /** Delete temp folder */
  @After
  public void tearDown() {
    Utility.deleteDirectory(directory);
    Configuration.instance()
        .temporaryDirectory(new File(System.getProperty("java.io.tmpdir")));
  }

  /** Test that the service can be instantiated */
  @Test
  public void testConstructor() {
    try {
      new PathsGeneration();
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail("Unable to instantiate the PathsGeneration service.");
    }
  }

  /**
   * Test that the service generates the output file
   */
  @Test
  public void testExample() {
    PathsGeneration generator = new PathsGeneration().example();
    AnnotatedService service = AnnotatedService.of(generator);
    service.configure(service.description());
    service.run();

    // check if output was created
    File file = new File(generator.outputFile.getAbsolutePath().toString());
    Assert.assertTrue(file.exists());
  }
}
